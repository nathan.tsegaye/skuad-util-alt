// example jsonString
// let jsonTransformString = {
//   remove: ["nest.fieldA", "google"],
//   map: {
//     "description": {
//       value: "metaInfo.startTime",
//       default: "2020-07-19T15:57:18.684Z",
//       action: "(new Date(value).toISOString()).slice(args[0],args[1])", // for actions one deafult argument "value" is added . In this case value will be  "metaInfo.description.id"
//       args: [0,7],
//     },
//     "metaInfo.description.title": {
//       value: "title.id",
//       default: "set me default",
//       action: "value + 'value' + 'lets see output'",// if title.id is undefined then the action will be performed on default value
//     },
//     "session.metaInfo": {  // new field can be created
//       value: "metaInfo.sessionInfo",
//     },
//     "metaInfo.staticValueExample":{
//       static: 100,
//     }
//   },
//   replace: true,
//   keep: ["metaInfo.a"], // this can  be used only with replace true
// };

let traverseToRemoveFields = (data, key) => {
  let count = 0;
  let nestedArr = key.split(".");
  let len = nestedArr.length;
  nestedArr.reduce((obj, key) => {
    count = count + 1;
    if (count == len) {
      delete obj && obj[key];
    }
    return obj[key] || {};
  }, data);
  return data;
};

let handleDeletion = (data, fields) => {
  for (let k of fields) {
    let arr = k.split(".");
    if (arr.length == 1) delete data[arr[0]];
    else traverseToRemoveFields(data, k);
  }
  return data;
};

let performValueMapping = (sourceData, targetData, mapping, destinationKey,originalObj) => {
  if (mapping.value) {
    let keyArr = mapping.value.split(".");
    let value;
    if (keyArr.length == 1) value = sourceData[keyArr[0]];
    else {
      let count = 1;
      let len = keyArr.length;
      value = keyArr.reduce((obj, key) => {
        if (!obj) return null;
        count = count + 1;
        if (count == len) {
          return obj[key];
        }
        return (obj && obj[key]) || null;
      }, sourceData);
    }
    if (value === null) value = mapping.default || null;
    if (mapping.action && value !== null) {
      value = new Function("value", "args", `return ${mapping.action}`)(
        value,
        {[mapping.args]:originalObj[mapping.args]}
      );
    }
    return value;
  }
  return mapping.default;
};

let handleNestedFieldMapping = (sourceData, targetData, keyArr, mapping,originalObj) => {
  let count = 0;
  let length = keyArr.length;
  let dataCopy = targetData;
  keyArr.reduce((targetData, key) => {
    count = count + 1;
    if (count == length) {
      targetData[key] =
        mapping.static ||
        performValueMapping(sourceData, dataCopy, { ...mapping }, key,originalObj);
    }
    if (!(key in targetData)) targetData[key] = {};
    return targetData[key];
    // new Function(val,`return ${a.otherAction}`)
  }, targetData);
  return targetData;
};

let handleMapFields = (sourceData, targetData, mapping,originalObj) => {
  for (let key of Object.keys(mapping)) {
    let arr = key.split(".");
    if (arr.length == 1) {
      targetData[key] =
        mapping[key].static ||
        performValueMapping(sourceData, targetData, mapping[key], key,originalObj);
    } else {
      handleNestedFieldMapping(sourceData, targetData, arr, mapping[key],originalObj);
    }
  }
};

let keepValues = (sourceData, targetData, fields,originalObj) => {
  for (let field of fields) {
    let keyArr = field.split(".");
    let count = 0;
    let length = keyArr.length;
    let dataCopy = targetData;
    keyArr.reduce((targetData, key) => {
      count = count + 1;
      if (count == length) {
        targetData[key] = performValueMapping(
          sourceData,
          dataCopy,
          { value: field },
          key,
          originalObj
        );
      }
      if (!(key in targetData)) targetData[key] = {};
      return targetData[key];
    }, targetData);
    return targetData;
  }
};
let evaluate = (jsonString, data) => {
  //! operations priority order -
  //!  1. map :[{MapOptions}]
  //!  2. remove :[String]
  //!  3. replace: Boolean
  //!  4. keep : [String]

  //!   Map Options
  //!  1. static (static option has highest priority)
  //!  2. value
  //!  3. deafult: if value is not found
  //!  4. action : perform action on  value/default given (takes a default argument value)
  //!  5. args :[String] addional static arguments can be defined
  if (jsonString.replace) targetData = {};
  else targetData = data;

  let originalObj = {...data}
  handleMapFields((sourceData = data), targetData, jsonString["map"],originalObj);

  if (jsonString.replace && jsonString.keep && jsonString.keep.length) {
    keepValues(data, targetData, jsonString["keep"],originalObj);
  }

  if (jsonString.remove && jsonString.remove.length > 1) {
    handleDeletion(data, jsonString["remove"],originalObj);
  }

  return targetData;
};

module.exports = { evaluate };
