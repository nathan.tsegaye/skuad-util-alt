'use strict';

const BaseModel = require('./BaseModel');
const { CoreService } = require('../apiWrappers/core');

module.exports = class BaseUser extends BaseModel {
    constructor(props) {
        super(props);
        this.service = global.Config.services.user;
        this.resource = '/api/v1/user'
        this.zpResource ='/api/v1/zp-user'
    }

    findByFirebaseId(firebaseId) {
        return CoreService.get(`${this.resource}/byFirebase/${firebaseId}`, null, {
            baseUrl: this.service,
            logger: this.logger
        })
    }

    findByEmail(email) {
        return CoreService.get(`${this.resource}/byEmail/${email}`, null, {
            baseUrl: this.service,
            logger: this.logger
        })
    }

    addMembership(firebaseUid, membership, options = {}) {
        if (options.headers && options.headers['authorization'])
            return CoreService.post(`${this.resource}/byFirebase/${firebaseUid}/membership`, membership, {
                baseUrl: this.service,
                logger: this.logger,
                ...options
            });
        return CoreService.post(`${this.resource}/membership`, { ...membership, uid: firebaseUid }, {
            baseUrl: this.service,
            logger: this.logger
        });
    }
    findAllByEmail(emails) {
        return CoreService.post(`${this.resource}/all/byEmail`, { emails }, {
            baseUrl: this.service,
            logger: this.logger
        })
    }
    revertMembership(firebaseUid,order_id,options = {}) {
      if (options.headers && options.headers['authorization']) {
        return CoreService.post(`${this.resource}/byFirebase/${firebaseUid}/revertMembership`, { firebaseUid, order_id }, {
            baseUrl: this.service,
            logger: this.logger,
            ...options
        });
      }
      return CoreService.post(`${this.resource}/revertMembership`, { firebaseUid, order_id }, {
        baseUrl: this.service,
        logger: this.logger,
        ...options
      });
    }
    validateReferralCode(code, options = {}) {
      return CoreService.get(`${this.resource}/referral/${code}`, null, {
        baseUrl: this.service,
        logger: this.logger,
        ...options
      })
    }
    validateTelecomUser(options = {}) {
      return CoreService.get(`${this.resource}/validate/telecomUser`, null, {
        baseUrl: this.service,
        logger: this.logger,
        ...options
      });
    }
    validateTelecomNumber(phone, options = {}) {
      return CoreService.get(`${this.resource}/validate/telecomNumber/${phone}`, null, {
        baseUrl: this.service,
        logger: this.logger,
        ...options
      });
    }
    addMembershipZP(firebaseUid, membership, options = {}) {
        if (options.headers && options.headers['authorization'])
            return CoreService.post(`${this.zpResource}/byFirebase/${firebaseUid}/membership`, membership, {
                baseUrl: this.service,
                logger: this.logger,
                ...options
            });
        return CoreService.post(`${this.zpResource}/membership`, { ...membership, uid: firebaseUid }, {
            baseUrl: this.service,
            logger: this.logger
        });
    }
};
