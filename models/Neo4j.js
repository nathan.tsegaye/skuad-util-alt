const BaseModel = require('./BaseModel');
const {CoreService} = require('../apiWrappers/core');
module.exports = class Neo4j extends BaseModel {
  constructor(props = {}) {
      super(props);
      this.service = global.Config.neo4j_host;
      this.resource = "/api/v1";
  }
  getLeafNodeCounts(nodeShortId,typeNode) {
      return CoreService.get(
          `${this.resource}/leafNodeCount/node/${nodeShortId}/${typeNode}`,
          {},
          {
              baseUrl: this.service,
              logger: this.logger
          }
      )
  }

  getNodeToTopLevelPath(nodeShortId, typeNode) {
      return CoreService.get(
        `${this.resource}/childToParent/child/${nodeShortId}/${typeNode}`,
          {},
          {
              baseUrl: this.service,
              logger: this.logger
          }
      )
  }

  getContentCount(nodeShortId, typePlan, contentType, contentVisibility) {
      return CoreService.get(
          `${this.resource}/getContentCount/node/${nodeShortId}/${typePlan}/content/${contentType}/${contentVisibility}`,
          {},
          {
              baseUrl: this.service,
              logger: this.logger
          }
      )
  }

  createNode(body) {
      return CoreService.post(
          `${this.resource}/insertNode`,
          body,
          {
              baseUrl: this.service,
              logger: this.logger
          }
      )
  }

  bulkInsertNode(body) {
    return CoreService.post(
        `${this.resource}/bulkInsert`,
          body,
          {
            baseUrl: this.service,
            logger: this.logger
          }
      )
  }
  updateNode(body) {
      return CoreService.post(
          `${this.resource}/updateNode`,
          body,
          {
              baseUrl: this.service,
              logger: this.logger
          }
      )
  }

  updateNodeContent(body) {
      return CoreService.post(
          `${this.resource}/updateNodeContent`,
          body,
          {
              baseUrl: this.service,
              logger: this.logger
          }
      )
  }

  detachNodeFromParents(body) {
    return CoreService.post(
        `${this.resource}/detachNodeFromParents`,
        body,
        {
            baseUrl: this.service,
            logger: this.logger
        }
    )
}

  async insertInNeo4j (learningPlanNeo4j)  {
    if(global.Config.env === 'testing') return 
      let count = 0;
      const maxTries = 3;
      while (true) {
        try {
          const response_neo = await this.createNode(learningPlanNeo4j);
          if (response_neo !== undefined) {
            if (response_neo.nodes_created === undefined && response_neo.nodes_created < 0) {
              throw new Error("Unable to create node in neo4j");
            }
            return;
          }
          else {
            throw new Error("Unable to create node in neo4j");
          }
        }
        catch(err) {
          if (++count==maxTries) {
            throw err;
          }
        }
      }
    }

    async updateNeo4jNode (updatedState) {
        if(global.Config.env === 'testing') return 
          let count = 0;
          const maxTries = 3;
          while (true) {
          try {
              const response_neo = await this.updateNode(updatedState);
              if (response_neo !== undefined) {
              if (response_neo.properties_updated === undefined && response_neo.properties_updated < 0) {
                  throw new Error("Unable to update node in neo4j");
              }
              return;
              }
              else {
              throw new Error("Unable to update node in neo4j");
              }

          }
          catch(err) {
              if (++count==maxTries)
              throw err;
              }
          }
      }


      async updateNeo4jContent (updatedContent) {
          if(global.Config.env === 'testing') return 
          let count = 0;
          const maxTries = 3;
          while (true) {
            try {
              const response_neo = await this.updateNodeContent(updatedContent);
              if (response_neo !== undefined) {
                if (response_neo.nodes_created === undefined && response_neo.new_content_added < 0) {
                  throw new Error("Unable to update node content in neo4j");
                }
                return;
              }
              else {
                throw new Error("Unable to update node content in neo4j");
              }

            }
            catch(err) {
              if (++count==maxTries) {
                throw err;
              }
            }
          }
        }

        async detachNodeFromParent (updatedContent) {
          if(global.Config.env === 'testing') return 

          let count = 0;
          const maxTries = 3;
          while (true) {
            try {
              const response_neo = await this.detachNodeFromParents(updatedContent);
              if (response_neo !== undefined) {
                if (response_neo.relations_removed === undefined) {
                  throw new Error("Unable to detach node from it's parents in neo4j");
                }
                return;
              }
              else {
                throw new Error("Unable to detach node from it's parents in neo4j");
              }

            }
            catch(err) {
              if (++count==maxTries) {
                throw err;
              }
            }
          }
        }

}
