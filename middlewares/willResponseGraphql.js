'use strict';
module.exports = ({ request, response, context, operation }) => {
    const { logger } = context;
    logger.fields['operationName'] = operation && operation.name && operation.name.value;
    logger.fields['respCode'] = 200;
    if (response.errors && response.errors.length) {
        let errorCode = 0;
        for (var error of response.errors) {
            let code = error.statusCode || (error.extensions && error.extensions.code);
            if (typeof code !== 'string' && code > errorCode) {
                errorCode = code;
            }
            if (typeof code === 'string' && code.length > 0 && parseInt(code)){
                errorCode = parseInt(code)
            }
            if (error.extensions && typeof error.extensions.code === 'string') {
                error.extensions.code = errorCode ? errorCode : 500;
            }
            if (typeof error.statusCode === 'string') {
                error.statusCode = errorCode ? errorCode : 500;
            }
        }
        if (logger.fields['respCode'] !== 401) logger.fields['respCode'] = 500;
        if (errorCode) logger.fields['respCode'] = errorCode;
        logger.fields['errors'] = response.errors
    }
};