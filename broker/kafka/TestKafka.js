var Kafka = require('node-rdkafka');
var producer = new Kafka.Producer({
  //'debug' : 'all',
  'metadata.broker.list': '35.195.118.91:9094',
  'dr_cb': true,  //delivery report callback
  'sasl.username': 'alz-user-staging',
  'sasl.password': '4tHD4EzyNfj4WLs6spBn',
  'security.protocol': 'SASL_PLAINTEXT',
  'sasl.mechanisms': 'PLAIN'
});
var topicName = 'dev-test';
//logging debug messages, if debug is enabled
producer.on('event.log', function(log) {
  console.log(log);
  console.log(">>>>>>>>>>>>")
});
//logging all errors
producer.on('event.error', function(err) {
  console.error('Error from producer');
  console.error(err);
});
//counter to stop this sample after maxMessages are sent
var counter = 0;
var maxMessages = 100;
producer.on('delivery-report', function(err, report) {
  console.log('delivery-report: ' + JSON.stringify(report));
  counter++;
});
//Wait for the ready event before producing
producer.on('ready', function(arg) {
  console.log('producer ready.' + JSON.stringify(arg));
  for (var i = 0; i < maxMessages; i++) {
    var value = Buffer.from('test-value-' +i);
    var key = "teste-"+i;
    // if partition is set to -1, librdkafka will use the default partitioner
    var partition = -1;
    var headers = [
      { header: "header value" }
    ]
    producer.produce(topicName, partition, value, key, Date.now(), "", headers);
  }
  //need to keep polling for a while to ensure the delivery reports are received
  var pollLoop = setInterval(function() {
      producer.poll();
      if (counter === maxMessages) {
        clearInterval(pollLoop);
        producer.disconnect();
      }
    }, 1000);
});
producer.on('disconnected', function(arg) {
  console.log('producer disconnected. ' + JSON.stringify(arg));
});
//starting the producer
producer.connect();